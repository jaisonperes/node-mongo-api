const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const PORT = 3000
const HOST = '0.0.0.0'

const app = express()

app.use(cors({ origin: '*' }))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

require('./app/controllers/index')(app)

app.get('/', function ({ req, res}) {
  res.send({
    routes: {
      products: '/products',
      stores: '/stores'
    }
  })
})

app.listen(PORT, HOST)