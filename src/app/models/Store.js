const mongoose = require('../../db')

const StoreSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
    unique: true
  },
  logo: {
    type: String,
    require: false
  },
  link: {
    type: String,
    require: true
  },
  products: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product'
  }],
  _created: {
    type: Date,
    default: Date.now
  },
  _updated: {
    type: Date,
    default: Date.now
  }
})

const Store = mongoose.model('Store', StoreSchema)

module.exports = Store