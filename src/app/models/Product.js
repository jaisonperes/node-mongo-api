const mongoose = require('../../db')

const ProductSchema = new mongoose.Schema({
  storeid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Store',
    required: true
  },
  store: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Store',
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  title: {
    type: String,
    require: true,
    unique: true
  },
  image: {
    type: String,
    require: false
  },
  link: {
    type: String,
    require: true
  },
  percentage: {
    type: Number,
    require: true
  },
  _created: {
    type: Date,
    default: Date.now
  },
  _updated: {
    type: Date,
    default: Date.now
  }
})

const Product = mongoose.model('Product', ProductSchema)

module.exports = Product