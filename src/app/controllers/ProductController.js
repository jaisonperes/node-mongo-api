const express = require('express')
const Product = require('../models/Product')
const router = express.Router()

// List all Products with their Store embedded
router.get('/', async (req, res) => {
  try {
    // Add cursor
    const { sortBy, search, limit, offset, descending } = req.query
    const descendingInt = descending && descending.toLowerCase() === 'true' ? -1 : 1
    const sortObj = sortBy ? { [sortBy]: descendingInt } : {}
    const limitInt = parseInt(limit) || 30
    const where = search && search.length ? JSON.parse(JSON.stringify(search)) : {}
    const offsetInt = parseInt(offset) || 0
    const count = await Product.countDocuments()

    const products = await Product.find(where)
      .sort(sortObj)
      .limit(limitInt)
      .skip(limitInt * offsetInt)
      .populate('store')
    return res.send({
      items: products,
      max: limitInt,
      total: products.length,
      pagination: {
        total: count || 0,
        page: offsetInt,
        pages: count > products.length ? Math.floor(count / products.length) : 1
      }
    })
    
  } catch (err) {
    return res.status(400).send({ error: 'Error on search products: '})
  }
})

// Return unique Product by Id with their Store embedded
router.get('/:productId', async (req, res) => {
  try {
    const product = await Product.findById(req.params.productId).populate('store')
    return res.send(product)

  } catch (err) {
    return res.status(400).send({ error: 'Error on search unique product'})
  }
})

// Create new Product
router.post('/', async (req, res) => {
  const { title } = req.body 
  try {
    // Check unique product by title
    if (await Product.findOne({ title }))
      return res.status(400).send({ error: 'Product already exists' })

    const product = await Product.create(req.body)
    return res.send(product)
  } catch (err) {
    return res.status(400).send({error: 'Error on create new product'})
  }
})

// Update Product by Id
router.put('/:productId', async (req, res) => {
  try {
    const product = await Product.findByIdAndUpdate(
      req.params.productId,
      req.body,
      { new: true }
    )
    product._updated = Date.now()
    return res.send(product)
  } catch (err) {
    return res.status(400).send({error: 'Error on update product'})
  }
})

// Remove Product by Id
router.delete('/:productId', async (req, res) => {
  try {
    await Product.findByIdAndRemove(req.params.productId)
    return res.send()
  } catch (err) {
    return res.status(400).send({ error: 'Error on delete product'})
  }
})

module.exports = app => app.use('/products', router)