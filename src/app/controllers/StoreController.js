const express = require('express')
const Store = require('../models/Store')
const Product = require('../models/Product')
const router = express.Router()

// List all Stores with their Products embedded
router.get('/', async (req, res) => {
  try {
    // Add cursor
    const { sortBy, search, limit, offset, descending } = req.query
    const descendingInt = descending && descending.toLowerCase() === 'true' ? -1 : 1
    const sortObj = sortBy ? { [sortBy]: descendingInt } : {}
    const limitInt = parseInt(limit) || 30
    const where = search && search.length ? JSON.parse(search) : {}
    const offsetInt = parseInt(offset) || 0
    const count = await Store.countDocuments()

    const stores = await Store.find()
      .sort(sortObj)
      .limit(limitInt)
      .skip(limitInt * offsetInt)
      .populate('products')
    return res.send({
      items: stores,
      max: limitInt,
      total: stores.length,
      pagination: {
        total: count || 0,
        page: offsetInt,
        pages: count > stores.length ? Math.floor(count / stores.length) : 1
      }
    })

  } catch (err) {
    return res.status(400).send({ error: 'Error on search stores'})
  }
})

// List one Store by Id with their Products embedded
router.get('/:storeId', async (req, res) => {
  try {
    const stores = await Store.findById(req.params.storeId).populate('products')
    return res.send(stores)

  } catch (err) {
    return res.status(400).send({ error: 'Error on search unique store'})
  }
})

// Create new Store with their Products
router.post('/', async (req, res) => {
  const { name, logo, link, products = [] } = req.body 
  try {
    // Check unique product by title
    if (await Store.findOne({ name }))
      return res.status(400).send({ error: 'Store already exists' })

    const store = await Store.create({ name, logo, link })

    await Promise.all(products.map(async product => {
      const storeProduct = new Product({...product, storeid: store._id})
      await storeProduct.save()
      store.products.push(storeProduct)
    }))

    await store.save()

    return res.send({ store })
  } catch (err) {
    console.log(err)
    return res.status(400).send({error: 'Error on create new store'})
  }
})

// Update one Store by Id with their Products
router.put('/:storeId', async (req, res) => {
  const { name, logo, link, products = [] } = req.body 
  try {
    const store = await Store.findByIdAndUpdate(req.params.storeId, {
      name,
      logo,
      link
    }, { new: true })

    // Update Store product list if request contains their list
    if(products && products.length > 0) {
      store.products = []
      await Product.remove({ storeid: store._id })

      await Promise.all(products.map(async product => {
        const storeProduct = new Product({...product, storeid: store._id, store: store._id})
        await storeProduct.save()
        store.products.push(storeProduct)
      }))

      await store.save()
    }
    
    store._updated = Date.now()
    
    return res.send({ store })
  } catch (err) {
    console.log(err)
    return res.status(400).send({error: 'Error on update store'})
  }
})

// Delete one Store by Id
router.delete('/:storeId', async (req, res) => {
  try {
    await Store.findByIdAndRemove(req.params.storeId)
    return res.send()
  } catch (err) {
    return res.status(400).send({ error: 'Error on delete store'})
  }
})

module.exports = app => app.use('/stores', router)