const mongoose = require('mongoose')

mongoose.connect('mongodb://mongo/lowprice')
mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
mongoose.set('useUnifiedTopology', true)
mongoose.Promise = global.Promise

module.exports = mongoose

