## NODE API

### NPM
```bash
$ npm install
$ node src/index.js
```
or

### Docker
```bash
$ docker-compose up
```

### Post new Store with their products

### POST on:
#### /stores/
```json
{
    "name": "StoreName",
    "logo": "store-logo.png",
    "link": "store-link",
    "products": [
        {
            "title": "SomeTitle",
            "image": "some-image.jpg",
            "link": "some-link",
            "price": 10,
            "percentage": 3,
            "storeId": "123412341243",
            "store": "123412341243"
        }
    ]
}
```

### Post new Product

### POST on:
#### /products/
```json
{
    "title": "SomeTitle",
    "image": "some-image.jpg",
    "link": "some-link",
    "price": 10,
    "percentage": 3,
    "storeId": "123412341243",
    "store": "123412341243"
}
```